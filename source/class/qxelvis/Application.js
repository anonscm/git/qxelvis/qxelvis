/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

/**
 * This is the main application class of your custom application "qxelvis"
 *
 */

 /**
  * This is the demo application for the qxelvis functionnalities.
  * @asset(qxelvis/*)
  */
qx.Class.define("qxelvis.Application",
{
  extend : qx.application.Standalone,



  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /**
     * This method contains the initial application code and gets called
     * during startup of the application
     */
    main : function()
    {
      // Call super class
      this.base(arguments);

      // Enable logging in debug variant
      if (qx.core.Environment.get("qx.debug"))
      {
        // support native logging capabilities, e.g. Firebug for Firefox
        qx.log.appender.Native;
        // support additional cross-browser console. Press F7 to toggle visibility
        qx.log.appender.Console;
      }

      /*
      -------------------------------------------------------------------------
        Below is your actual application code...
      -------------------------------------------------------------------------
      */

      qxelvis.session.service.Session.SERVICE_BASE_URL = "/elvis/cgi-bin/";
      qxelvis.session.service.Session.SESSION_ID_KEY = "qx_elvis_demo_session";
      var session = qxelvis.session.service.Session.getInstance();
      var sessionId = session.getSessionId();
      //var grpsI = null;

      session.addListener("gotUserInfo", function(e) {
        this.debug(qx.lang.Json.stringify(e.getData()));
      }, this);
      session.addListener("gotGroupsInfo", function(e) {
        //grpsI = new qxelvis.session.data.Groups(e.getData());
        this.debug(qx.lang.Json.stringify(e.getData()));
        //this.debug(grpsI.getReadGroupIds());
        //this.debug(grpsI.getWriteGroupIds());
        //this.debug(grpsI.getAllGroupIds());
      }, this);

      var label1 = new qx.ui.basic.Label("NA");
      var logoutBtn = new qx.ui.form.Button("Logout");
      var usrInfo = new qx.ui.menubar.Button("User");
      var grpInfo = new qx.ui.menubar.Button("Groups");
      var button1 = new qx.ui.menubar.Button("UUID");
      var button2 = new qx.ui.menubar.Button("sha512");
      var logLbl = new qx.ui.basic.Label().set({
        rich : true,
        width: 400,
        height: 300,
        backgroundColor: "white"
      });

      usrInfo.addListener("execute", function() {
        this.debug("click usrInfo");
      }, this);

      var loginForm = new qxelvis.ui.LoginForm();

      var menu = new qx.ui.menubar.MenuBar();
      var loginBtn = new qxelvis.ui.menubar.UserButton();
      menu.add(usrInfo);
      menu.add(grpInfo);
      menu.add(button1);
      menu.add(button2);
      menu.add(new qx.ui.core.Spacer(), {flex: 1});
      menu.add(loginBtn);

      var loginWin = new qx.ui.window.Window("Login");
      loginWin.setLayout(new qx.ui.layout.Grow());
      loginWin.add(loginForm);
      loginWin.setModal(true);
      loginBtn.addListener("execute", function() {
        if (! session.userLoggedIn()) {
          session.addListenerOnce("authenticationSucced", function() {
            this.close();
          }, loginWin);
          loginWin.center();
          loginWin.show();
        }
      }, this);

      loginForm.addListener("reset", function() {
        loginWin.close();
      }, this);

      // Filterable combo box
      var fcbData = [
        {"label" : "Pomme", "id" : 1},
        {"label" : "Orange", "id" : 2},
        {"label" : "Banane", "id" : 3},
        {"label" : "Kiwi", "id" : 4},
        {"label" : "Mangue", "id" : 5},
        {"label" : "Noix de Coco", "id" : 6}
      ];
      var fcb = new qxelvis.ui.FilteredComboBox(fcbData);
      fcb.setPlaceholder("FilteredComboBox");
      var fcbList = fcb.getChildControl('dropdown').getChildControl('list');
      fcb.addListener("changeSelection", function(e) {
        this.debug("FilteredComboBox: selection has changed : " + e.getData().getLabel() + " [" + e.getData().getId() + "]");
        //console.log(e.getData());
      }, this);

      // VirtualComboBox
      var vcbData = [
        'pomme',
        'orange',
        'banane',
        'kiwi',
        'mangue',
        'noix de coco'
      ];
      var vcb = new qx.ui.form.VirtualComboBox(new qx.data.Array(vcbData));
      vcb.setPlaceholder("VirtualComboBox");

      // Contributor selector
      var cs = new qxelvis.ui.users.ContributorsSelector();

      // GroupList chooser

      var gl = new qxelvis.ui.groups.GroupList();

      // Document is the application root
      var doc = this.getRoot();

      // Menu
      doc.add(menu, {right: 0, top: 0, left: 0});
      // Logout button
      doc.add(logoutBtn, {right: 10, top: 30});
      // Log label
      doc.add(logLbl, {left: 10, top: 30});
      // qxelvis.ui.FilteredComboBox
      doc.add(fcb, {right: 10, top: 60});
      doc.add(vcb, {right: 10, top: 90});
      // GroupList
      gl.setWidth(200);
      gl.setHeight(300);
      doc.add(gl, {left : 430, top : 30});
      
      doc.add(cs, {left : 650, top : 30});

      button1.addListener("execute", function(e) {
        this.debug("generate new UUID");
        logLbl.setValue(qxelvis.util.UUID.generate());
      }, this);

      button2.addListener("execute", function(e) {
        this.debug("Call sha512");
        logLbl.setValue(qxelvis.util.Sha512.sha512('bla'));
      }, this);

      usrInfo.addListener("execute", function(e) {
        this.debug("request user info");
        session.requestUserInfo();
      }, this);

      grpInfo.addListener("execute", function(e) {
        this.debug("request group info");
        session.requestGroupsInfo();
      }, this);

      session.addListener("newSalt", function(e) {
        this.debug("New salt: " + e.getData() + " (pepper is: " + session.getPepper() + ")");
      }, this);

      logoutBtn.addListener("execute", function(e) {
        session.userLogout();
      }, this);

      loginForm.addListener("validate", function(e) {
        //this.debug(qx.lang.Json.stringify(e.getData()));
        var lg = e.getData()['username'];
        var pw = e.getData()['password'];
        session.userLogin(lg, pw);
      }, this);

      session.addListener("authenticationFailed", function() {
        this.debug("Invalid login");
        loginForm.setValid(false);
      }, this);

      session.addListener("authenticationSucced", function() {
        this.debug("Valid login");
        loginForm.reset();
      }, this);

      session.addListener("gotUserInfo", function(e) {
        logLbl.setValue(this.stringify(e.getData()));
      }, this);

      session.addListener("gotGroupsInfo", function(e) {
        logLbl.setValue(this.stringify(e.getData()));
        /*
        session.getUserGroupsInfo().addListener("changeGroupCanRead", function(e) {
          this.debug("New read groups: " + qx.lang.Json.stringify(e.getData()));
        }, this);
        session.getUserGroupsInfo().addListener("changeGroupCanWrite", function(e) {
          this.debug("New write groups: " + qx.lang.Json.stringify(e.getData()));
        }, this);
        */
        //var a = new qx.data.Array(e.getData());
        //var a = qx.data.marshal.Json.createModel(e.getData());
        //gl.setGroups(a);
      }, this);

      //this.info(qxelvis.util.Sha512.sha512("blabla"));
    },

    /**
     * Use the qx.lang.Json.stringify methode and make its output HTML friendly.
     * @param str {Data} The data structure to strigify.
     * @return {String} A Json representation of str in HTML format.
     */
    stringify : function(str) {
      return qx.lang.Json.stringify(str, null, ' ').replace(/ /ig, '&nbsp').replace(/\n/ig, '<br />');
    }
  }
});
