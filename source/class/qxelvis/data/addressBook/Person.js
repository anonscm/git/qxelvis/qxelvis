/**
 * The qxelvis.data.plant.Person
 */
qx.Class.define("qxelvis.data.addressBook.Person", {
  extend : qx.core.Object,
  implement : qxelvis.data.addressBook.IPerson,
  include: [qxelvis.data.addressBook.MPerson],
  construct : function(
      id = null,
      firstname = null,
      lastname = null,
      address1 = null,
      address2 = null,
      city = null,
      state = null,
      postalcode = null,
      contryId = null,
      email = null,
      phone = null,
      fax = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setFirstname(firstname);
    this.setLastname(lastname);
    this.setAddress1(address1);
    this.setAddress2(address2);
    this.setCity(city);
    this.setState(state);
    this.setPostalcode(postalcode);
    this.setContryId(contryId);
    this.setEmail(email);
    this.setPhone(phone);
    this.setFax(fax);
  }
});
