/**
 * The qxelvis.data.plant.IPerson interface
 */
qx.Interface.define("qxelvis.data.addressBook.IPerson", {
  properties : {
    id : {},
    firstname : {},
    lastname : {},
    address1 : {},
    address2 : {},
    city : {},
    state : {},
    postalcode : {},
    contryId : {},
    email : {},
    phone : {},
    fax : {}
  }
});
