/**
 * The qxelvis.data.plant.Place
 */
qx.Class.define("qxelvis.data.plant.Place", {
  extend : qx.core.Object,
  implement : qxelvis.data.plant.IPlace,
  include: [qxelvis.data.plant.MPlace],
  construct : function(
      id = null,
      lotId = null,
      siteId = null,
      plantationDate = null,
      removingDate = null,
      locationList = []
    ) {
    this.base(arguments);
    this.setId(id);
    this.setLotId(lotId);
    this.setSiteId(siteId);
    this.setPlantationDate(plantationDate);
    this.setRemovingDate(removingDate);
    this.setLocationList(locationList);
  }
});
