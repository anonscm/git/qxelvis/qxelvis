/**
 * The qxelvis.data.plant.IVariety interface
 */
qx.Interface.define("qxelvis.data.plant.IVariety", {
  properties : {
    id : {},
    breeder : {},
    breederId : {},
    comment : {},
    taxonomyId : {},
    genus : {},
    specie : {},
    editor : {},
    editorId : {},
    listVarietyName : [],
  }
});
