/**
 * The qxelvis.data.plant.IAccession interface
 */
qx.Interface.define("qxelvis.data.plant.IAccession", {
  properties : {
    id : {},
    introductionName : {},
    varietyId : {},
    comment : {},
    introductionCloneId : {},
    provider : {},
    providerId : {},
    introductionDate : {},
    collectionDate : {},
    collectionSiteId : {}
  }
});
