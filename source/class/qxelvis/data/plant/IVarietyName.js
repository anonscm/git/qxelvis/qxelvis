/**
 * The qxelvis.data.plant.IVarietyName interface
 */
qx.Interface.define("qxelvis.data.plant.IVarietyName", {
  properties : {
    id : {},
    varietyId : {},
    value : {},
    date : {},
    varietyNameTypeId : {},
    type : {},
  }
});
