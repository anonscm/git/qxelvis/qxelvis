/**
 * The qxelvis.data.plant.ILot interface
 */
qx.Interface.define("qxelvis.data.plant.ILot", {
  properties : {
    id : {},
    type : {},
    name : {},
    accessionId : {},
    lotSourceId : {},
    multiplicationDate : {},
    harvestingDate : {},
    creationDate : {},
    destructionDate : {},
    quantity : {},
    place : {}
  }
});
