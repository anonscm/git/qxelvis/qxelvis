/**
 * The qxelvis.data.plant.IPlace interface
 */
qx.Interface.define("qxelvis.data.plant.IPlace", {
  properties : {
    id : {},
    lotId : {},
    siteId : {},
    plantationDate : {},
    removingDate : {},
    locationList : []
  }
});
