/**
 * The qxelvis.data.plant.Accession
 */
qx.Class.define("qxelvis.data.plant.Accession", {
  extend : qx.core.Object,
  implement : qxelvis.data.plant.IAccession,
  include: [qxelvis.data.plant.MAccession],
  construct : function(
      id = null,
      introductionName = null,
      varietyId = null,
      comment = null,
      introductionCloneId = null,
      provider = null,
      providerId = null,
      introductionDate = null,
      collectionDate = null,
      collectionSiteId = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setComment(comment);
    this.setIntroductionName(introductionName);
    this.setVarietyId(varietyId);
    this.setIntroductionCloneId(introductionCloneId);
    this.setProviderId(providerId);
    this.setProvider(provider);
    this.setIntroductionDate(introductionDate);
    this.setCollectionDate(collectionDate);
    this.setCollectionSiteId(collectionSiteId);

  }
});
