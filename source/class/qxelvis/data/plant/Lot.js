/**
 * The qxelvis.data.plant.Lot
 */
qx.Class.define("qxelvis.data.plant.Lot", {
  extend : qx.core.Object,
  implement : qxelvis.data.plant.ILot,
  include: [qxelvis.data.plant.MLot],
  construct : function(
      id = null,
      type = null,
      name = null,
      accessionId = null,
      lotSourceId = null,
      multiplicationDate = null,
      harvestingDate = null,
      creationDate = null,
      destructionDate = null,
      quantity = null,
      place = null
    ) {
    this.base(arguments);
    this.setId(id);
    this.setType(type);
    this.setName(name);
    this.setAccessionId(accessionId);
    this.setLotSourceId(lotSourceId);
    this.setMultiplicationDate(multiplicationDate);
    this.setHarvestingDate(harvestingDate);
    this.setCreationDate(creationDate);
    this.setDestructionDate(destructionDate);
    this.setQuantity(multiplicationDate);
    this.setPlace(place);
  }
});
