/**
 * The qxelvis.data.notation.INotation interface
 */
qx.Interface.define("qxelvis.data.notation.INotation", {
  properties : {
    id : {},
    value : {},
    date : {},
    comment : {},
    siteId : {},
    type : {},
    observer : {}
  }
});
