/**
 * The qxelvis.data.notation.MTypeNotation mixin
 */
qx.Mixin.define("qxelvis.data.notation.MTypeNotation", {
  properties : {
    name : {
      nullable : true,
      event : "changeName"
    },
    constraint : {
      nullable : true,
      event : "changeConstraint"
    },

    type : {
      event : "changeType"
    },
    list : {
      nullable : true,
      event : "changeList"
    }
  }
});
