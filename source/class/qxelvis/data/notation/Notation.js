/**
 * The qxelvis.data.notation.Notation
 */
qx.Class.define("qxelvis.data.notation.Notation", {
  extend : qx.core.Object,
  implement : qxelvis.data.notation.INotation,
  include: [qxelvis.data.notation.MNotation],
  construct : function(value, type, date, id = null, observer = null, comment = null, siteId = null) {
    this.base(arguments);
    this.setValue(value);
    this.setType(type);
    this.setDate(date);
    this.setId(id);
    this.setObserver(observer);
    this.setComment(comment);
    this.setSiteId(siteId);
  }
});
