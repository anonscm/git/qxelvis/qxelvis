/**
 * The qxelvis.data.notation.MNotation mixin
 */
qx.Mixin.define("qxelvis.data.notation.MNotation", {
  properties : {
    id : {
      nullable : true,
      event : "changeId"
    },
    value : {
      event : "changeValue"
    },
    date : {
      event : "changeDate"
    },
    comment : {
      nullable : true,
      event : "changeComment"
    },
    siteId : {
      nullable : true,
      event : "changeSiteId"
    },
    type : {
      event : "changeType"
    },
    observer : {
      nullable : true,
      event : "changeObserver"
    }
  }
});
