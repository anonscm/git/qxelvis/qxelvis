/**
 * The qxelvis.data.notation.ITypeNotation interface
 */
qx.Interface.define("qxelvis.data.notation.ITypeNotation", {
  properties : {
    name : {},
    type : {},
    constraint : {},
    list : [],
  }
});
