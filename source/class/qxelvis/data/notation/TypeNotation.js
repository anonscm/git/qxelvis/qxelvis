/**
 * The qxelvis.data.notation.TypeNotation
 * 14 Mai 2019
 * Fabrice Dupuis
 */
qx.Class.define("qxelvis.data.notation.TypeNotation", {
  extend : qx.core.Object,
  implement : qxelvis.data.notation.ITypeNotation,
  include: [qxelvis.data.notation.MTypeNotation],
  construct : function(name, type, constraint = null, list = null) {
    this.base(arguments);
    this.setName(name);
    this.setType(type);
    this.setConstraint(constraint);
    this.setList(list);
  }
});
