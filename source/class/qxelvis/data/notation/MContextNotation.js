/**
 * The qxelvis.data.notation.MContextNotation mixin
 */
qx.Mixin.define("qxelvis.data.notation.MContextNotation", {
  properties : {
    name : {
      event : "changeName"
    },
    comment : {
      nullable : true,
      event : "changeConstraint"
    },

    listUserGroup : {
      nullable : true,
      event : "changeType"
    },
    listTypeNotation : {
      nullable : true,
      event : "changeList"
    }
  }
});
