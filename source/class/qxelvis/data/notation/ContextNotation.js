/**
 * The qxelvis.data.notation.ContextNotation
 * 14 Mai 2019
 * Fabrice Dupuis
 */
qx.Class.define("qxelvis.data.notation.ContextNotation", {
  extend : qx.core.Object,
  implement : qxelvis.data.notation.IContextNotation,
  include: [qxelvis.data.notation.MContextNotation],
  construct : function(name, comment, listUserGroup = null, listTypeNotation = null) {
    this.base(arguments);
    this.setName(name);
    this.setComment(comment);
    this.setListUserGroup(listUserGroup);
    this.setListTypeNotation(listTypeNotation);
  }
});
