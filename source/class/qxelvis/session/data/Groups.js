/**
 * The qxelvis.session.data.Groups
 *
 * This class can store user groups informations and read or write them
 * to the local storage of the navigator.
 *
 * @ignore(Map)
 */
qx.Class.define("qxelvis.session.data.Groups", {
  extend : qx.core.Object,
  /**
   * @param gi {Array} A Array of Dict of the form
   * <pre class='javascript'>
   * [
   *   {
   *     'id' : '1',
   *     'name' : 'Groupe1',
   *     'description' : 'The first user group',
   *     'active' : true,
   *     'read' : true,
   *     'write' : false
   *   },
   *   ...
   * ]
   * </pre>
   */
  construct : function(gi) {
    this.base(arguments);
    // TODO check the content of gi: write a method to do the ckecking
    if (gi && Array.isArray(gi)) {
      this.setGroupsInfos(gi);
    } else {
      this.__groups = new Array();
    }
    this.addListener("changeGroupCanRead", function(e) {
      this.fireEvent("changeGroupRights");
    }, this);
    this.addListener("changeGroupCanWrite", function(e) {
      this.fireEvent("changeGroupRights");
    }, this);
  },

  events : {
    /**
     * Fired when a read right have changed for a group.
     *
     * The data is the list of group id with read right.
     */
    'changeGroupCanRead' : 'qx.event.type.Data',

    /**
     * Fired when a write right have changed for a group.
     *
     * The data is the list of group id with write right.
     */
    'changeGroupCanWrite' : 'qx.event.type.Data',

    /**
     * Fired when group rights have changed.
     */
    'changeGroupRights' : 'qx.event.type.Event'
  },

  members : {
    /**
     * Holds the groups list
     */
    __groups : null,

    /**
     * Set the group list from a array of dict.
     *
     * @param gi {Array} a Array of Dict (see constructor for details)
     */
    setGroupsInfos : function(gi) {
      qx.core.Assert.assertArray(gi, 'gi must be a Array of group information');
      if (gi && Array.isArray(gi)) {
        this.__groups = Array.from(gi);
      } else {
        this.error('gi must be a Array');
      }
      this.__groups.forEach(function(item, index, array) {
        item['mask'] = 3;
        //item['read'] = true;
        //item['write'] = true;
      });
      this.applyUserPreferences();
    },

    /**
     * Apply the user preference on read/write permission on the groups
     */
    applyUserPreferences : function() {
      var session = qxelvis.session.service.Session.getInstance();
      var prefKey = session.getPreferenceId();

      var store = qx.bom.Storage.getLocal();
      var readG = store.getItem('readGroups');
      var writeG = store.getItem('writeGroups');
      var pref = store.getItem(prefKey);
      var masks = new Map();
      if (Array.isArray(readG)) {
        readG.forEach(function(item) {
        //for (const item of readG) {
          masks[item] |= 1;
        });
      }
      if (Array.isArray(writeG)) {
        writeG.forEach(function(item) {
          masks[item] |= 2;
        });
      }
      if (pref != null) {
        masks = pref['masks'];
      }
      //this.debug("loaded masks: " + qx.lang.Json.stringify(masks));
      this.setGroupsRights(masks);
    },

    /**
     * Save the user preferences on read/write permission
     */
    saveUserPreferences : function() {
      var readG = this.getReadGroupIds();
      var writeG = this.getWriteGroupIds();
      var store = qx.bom.Storage.getLocal();
      store.setItem('readGroups', readG);
      store.setItem('writeGroups', writeG);
      //this.debug("Saved read groups: " + qx.lang.Json.stringify(readG));
      //this.debug("Saved write groups: " + qx.lang.Json.stringify(writeG));

      var session = qxelvis.session.service.Session.getInstance();
      var prefKey = session.getPreferenceId();
      var masks = {};
      masks['masks'] = this.getGroupsRights();
      store.setItem(prefKey, masks);
    },

    /**
     * Get a Array of groups ids with write access right.
     *
     * @return {Array} A Array of integer
     */
    getWriteGroupIds : function() {
      var g = new Array();
      this.__groups.forEach(function(item, index, array) {
        //if (item['write']) {
        if (item['mask'] & 2) {
          g.push(item['id']);
        }
      });
      return g;
    },

    /**
     * Get a Array of groups ids with read access right.
     *
     * @return {Array} A Array of integer
     */
    getReadGroupIds : function() {
      var g = new Array();
      this.__groups.forEach(function(item, index, array) {
        //if (item['read']) {
        if (item['mask'] & 1) {
          g.push(item['id']);
        }
      });
      return g;
    },

    /**
     * Get a Array of all groups ids accessible for the current user.
     *
     * @return {Array} A Array of integer
     */
    getAllGroupIds : function() {
      var g = new Array();
      this.__groups.forEach(function(item, index, array) {
        g.push(item['id']);
      });
      return g;
    },

    /**
     * Get the groups read/write rights
     *
     * @return {Map} map containing pairs of gid:mask
     */
    getGroupsRights : function() {
      var r = new Map();
      this.__groups.forEach(function(item, index, array) {
        r[item['id']] = item['mask'];
      });
      return r;
    },

    /**
     * Set the groups read/write rights
     *
     * @param masks {Map} map containing pairs of gid:mask
     */
    setGroupsRights : function(masks) {
      //qx.core.Assert.assertMap(masks);
      var readChange = false;
      var writeChange = false;
      this.__groups.forEach(function(item, index, array) {
        if (masks[item['id']] != null) {
          if ((item['mask'] & 1) != (masks[item['id']] & 1)) {
            readChange = true;
          }
          if ((item['mask'] & 2) != (masks[item['id']] & 2)) {
            writeChange = true;
          }
          item['mask'] = masks[item['id']];
        }
      });
      if (readChange) {
        this.fireDataEvent('changeGroupCanRead', this.getReadGroupIds());
      }
      if (writeChange) {
        this.fireDataEvent('changeGroupCanWrite', this.getWriteGroupIds());
      }
    },

    /**
     * Get a group information given its id
     *
     * @param gid {Integer} the group id
     * @return {Map} the group information as a Map
     */
    getGroup : function(gid) {
      qx.core.Assert.assertInteger(gid, 'gid must be integer');
      var g = this.__groups.find(function(elmt) {
        return elmt['id'] == gid;
      });
      if (! g) {
        this.info('gid [' + gid + '] not found in group list');
      }
      return g;
    },

    /**
     * Get the read right for a group
     *
     * @param gid {Integer} the group id
     * @return {Boolean} the value for the right
     */
    getGroupCanRead : function(gid) {
      qx.core.Assert.assertInteger(gid, 'gid must be integer');
      var g = this.getGroup(gid);
      if (g) {
        return Boolean(g['mask'] & 1);
      }
      return false;
    },

    /**
     * Set the read right for a group
     *
     * @param gid {Integer} the group id
     * @param val {Boolean} the value of the right
     */
    setGroupCanRead : function(gid, val) {
      qx.core.Assert.assertInteger(gid, 'gid must be integer');
      qx.core.Assert.assertBoolean(val, 'val must be boolean');
      var g = this.getGroup(gid);
      if (g && (g['mask'] & 1) != val) {
        g['mask'] ^= 1;
        this.fireDataEvent('changeGroupCanRead', this.getReadGroupIds());
      }
    },

    /**
     * Get the write right for a group
     *
     * @param gid {Integer} the group id
     * @return {Boolean} the value for the right
     */
    getGroupCanWrite : function(gid) {
      qx.core.Assert.assertInteger(gid, 'gid must be integer');
      var g = this.getGroup(gid);
      if (g) {
        return Boolean(g['mask'] & 2);
      }
      return false;
    },

    /**
     * Set the write right for a group
     *
     * @param gid {Integer} the group id
     * @param val {Boolean} the value of the right
     */
    setGroupCanWrite : function(gid, val) {
      qx.core.Assert.assertInteger(gid, 'gid must be integer');
      qx.core.Assert.assertBoolean(val, 'val must be boolean');
      var g = this.getGroup(gid);
      if (g && (g['mask'] & 2) != val) {
        g['mask'] ^= 2;
        this.fireDataEvent('changeGroupCanWrite', this.getWriteGroupIds());
      }
    }
  }
});
