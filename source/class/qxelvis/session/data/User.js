/**
 * The qxelvis.session.data.User
 */
qx.Class.define("qxelvis.session.data.User", {
  extend : qx.core.Object,
  implement : qxelvis.session.data.IUser,
  include: [qxelvis.session.data.MUser],
  construct : function() {
    this.base(arguments);
  },
  members : {
    /**
     * Get a name to display in UI
     *
     * Returns FirstName if it exists or Login
     */
    getDisplayName : function() {
      var f = this.getFirstName();
      if (f)
        return f;
      return this.getLogin();
    },

    /**
     * Get a full name
     *
     * Returns the FirstName and the LastName separated by a space.
     */
    getFullName : function() {
      var n = [this.getFirstName(), this.getLastName()];
      return n.join(' ');
    }
  }
});
