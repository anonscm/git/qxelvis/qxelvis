/**
 * The qxelvis.session.data.IUser interface
 */
qx.Interface.define("qxelvis.session.data.IUser", {
  properties : {
    id : {},
    login : {},
    firstName : {},
    lastName : {},
    email : {},
    phone : {}
  }
});
