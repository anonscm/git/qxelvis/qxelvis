/**
 * The qxelvis.session.data.MUser mixin
 */
qx.Mixin.define("qxelvis.session.data.MUser", {
  properties : {
    id : {
      event : "changeId"
    },
    login : {
      event : "changeLogin"
    },
    firstName : {
      nullable : true,
      event : "changeFirstName"
    },
    lastName : {
      nullable : true,
      event : "changeLastName"
    },
    email : {
      nullable : true,
      event : "changeEmail"
    },
    phone : {
      nullable : true,
      event : "changePhone"
    }
  }
});
