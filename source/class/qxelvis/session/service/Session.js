/**
 * Service that manage an application session
 *
 * The intended purpose of qxelvis.session.service.Session is to easily manage
 * data persistance for session for applications that needs to use login
 * mecanisms and user/group access rights.
 *
 * *Usage example*
 *
 * <pre class='javascript'>
 *   // Define a key to identify the application
 *   qxelvis.session.service.Session.SESSION_ID_KEY = "qx_elvis_demo_session";
 *   // Get a qxelvis.session.service.Session instance
 *   var session = qxelvis.session.service.Session.getInstance();
 * <pre>
 */
qx.Class.define("qxelvis.session.service.Session",
{
  type : "singleton",
  extend : qx.core.Object,
 
  events : {
    /**
     * Fired when Session ID has been renewed.
     *
     * This event contains the current session ID.
     */
    "changeSessionId" : "qx.event.type.Data",

    /**
     * Fired when communication with the server failed.
     *
     * This event contains the data of the qx.io.remote.Rpc 'failed' event.
     */
    "connectionFailed" : "qx.event.type.Data",

    /**
     * Fired when connection to the server succed.
     *
     * This event contains a dictionnary with keys:
     *  - ip      : the ip of the client seen by the server
     *  - salt    : the current salting value for authentication
     *  - user_id : the user id registered for this session or null
     *  - id      : the current application id
     */
    "connectionSucced" : "qx.event.type.Data",

    /**
     * Fired when user logged out.
     */
    "userLoggedOut" : "qx.event.type.Event",

    /**
     * Fired when user info request succed.
     */
    "gotUserInfo" : "qx.event.type.Data",

    /**
     * Fired when groups info request succed.
     */
    "gotGroupsInfo" : "qx.event.type.Data",

    /**
     * Fired when a new salt value is generated.
     */
    "newSalt" : "qx.event.type.Data",

    /**
     * Fired when user authentication fail.
     */
    "authenticationFailed" : "qx.event.type.Event",

    /**
     * Fired when user authentication succed.
     */
    "authenticationSucced" : "qx.event.type.Event"
  },

  /**
   * Constructor
   */
  construct : function() {
    this.base(arguments);
    this.connect();
    this.addListener("gotUserInfo", function(e) {
      this.requestGroupsInfo();
    }, this);
    this.requestUserInfo();
    //this.requestGroupsInfo();
  },

  statics : {
    SERVICE_BASE_URL : "/cgi-bin/elvis/",
    APPLICATION_ID_KEY : "qx_elvis_default_application",
    SESSION_ID_KEY : "qx_elvis_default_session"
  },

  /**
   * Method definitions.
   */
  members : {
    /**
     * Private member where the session ID is stored for this session.
     */
    __sessionId : null,

    /**
     * Private member storing the pepper value.
     */
    __pepper : null,

    /**
     * Private member storing the user logging state value.
     */
    __userLoggedIn : false,

    /**
     * Pepper access.
     *
     * @return {String} The pepper value.
     */
    getPepper : function() {
      return this.__pepper;
    },

    __userInfo : null,
    getUserInfo : function() {
      return this.__userInfo;
    },
    userLoggedIn : function() {
      if (this.__userInfo) {
        return true;
      }
      return false;
    },

    __userGroupsInfo : null,
    getUserGroupsInfo : function() {
      return this.__userGroupsInfo;
    },

    /**
     * Get the session ID
     *
     * Get the session ID stored in LocalStorage under the key
     * qxelvis.session.service.Session.SESSION_ID_KEY or create
     * a new one and store if in LocalStorage.
     *
     * @returns {String} the session ID as a UUID string.
     */
    getSessionId : function() {
      var sessionIdKey = qxelvis.session.service.Session.SESSION_ID_KEY;
      this.__sessionId = qx.module.Storage.getLocalItem(sessionIdKey);
      if (! this.__sessionId) {
        this.renewSessionId();
      }
      return this.__sessionId;
    },

    /**
     * Renew the session ID
     *
     * @return {String} The session ID
     */
    renewSessionId : function() {
      var sessionIdKey = qxelvis.session.service.Session.SESSION_ID_KEY;
      this.__sessionId = qxelvis.util.UUID.generate();
      qx.module.Storage.setLocalItem(sessionIdKey, this.__sessionId);
      this.fireDataEvent("changeSessionId", this.__sessionId);
      return this.__sessionId;
    },

    /**
     * Connect the application to the web services
     *
     * Transfers the session ID to the server and get back informations as
     * client IP, user ID, current salt value and session ID currently
     * stored for this session ID.
     */
    connect : function() {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "ServiceSession.py", "ServiceSession");
      rpc.addListener("failed", function(e) {
        this.error(e.getData().toString());
        this.fireDataEvent("connectionFailed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        if (e.getData()["result"] == false) {
          this.warn("Connection failed try to reconnect...");
          this.renewSessionId();
          this.connect();
        } else {
          this.__pepper = e.getData()["result"]["pepper"];
          this.fireDataEvent("connectionSucced", e.getData());
        }
      }, this);
      var sessionId = this.getSessionId();
      rpc.callAsyncListeners(true, "sessionConnect", sessionId);
    },

    /**
     * Get user info
     */
    requestUserInfo : function() {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "ServiceSession.py", "ServiceSession");
      rpc.setServerData({"sessionId" : this.getSessionId()}); // Test for ServerData
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("connectionFailed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.__setUserInfo(e.getData()['result']);
        this.fireDataEvent("gotUserInfo", e.getData()['result']);
      }, this);
      rpc.callAsyncListeners(true, "getUserInfos", this.getSessionId());
    },

    /**
     * Set the UserInfo
     */
    __setUserInfo : function(userData) {
      if (userData != null) {
        if (this.__userInfo == null) {
          this.__userInfo = new qxelvis.session.data.User();
        }
        var user = this.__userInfo;
        user.setId(userData["id"]);
        user.setLogin(userData["login"]);
        user.setFirstName(userData["firstname"]);
        user.setLastName(userData["lastname"]);
        user.setEmail(userData["email"]);
        user.setPhone(userData["phone"]);
        this.debug("user is: " + user.getLogin()
          + " (" + user.getLastName() + " " + user.getFirstName() + ")");
      } else {
        this.__userInfo = null;
        this.debug("no user");
      }
    },

    /**
     * Get usergroups for the current logged user
     */
    requestGroupsInfo : function() {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "ServiceSession.py", "ServiceSession");
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("connectionFailed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.__setGroupsInfo(e.getData()['result']);
        this.fireDataEvent("gotGroupsInfo", e.getData()['result']);
      }, this);
      rpc.callAsyncListeners(true, "getUserGroups", this.getSessionId());
    },

    /**
     * Set the GroupsInfo from a Array
     */
    __setGroupsInfo : function(gi) {
      if (gi != null) {
        if (this.__userGroupsInfo == null) {
          var grps = new qxelvis.session.data.Groups(gi);
          this.__userGroupsInfo = grps;
        } else {
          this.__userGroupsInfo.setGroupsInfos(gi);
        }
        this.debug("user belongs to " + this.__userGroupsInfo.getAllGroupIds().length + " groups");
      } else {
        this.__userGroupsInfo = null;
        this.debug("no groups");
      }
    },

    /**
     * Connect a user.
     */
    userLogin : function(lg, pw) {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "ServiceSession.py", "ServiceSession");
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("connectionFailed", e.getData());
      }, this);
      rpc.addListenerOnce("completed", function(e) {
        var salt = e.getData()['result'];
        var cred = lg + this.getPepper() + pw;
        cred = qxelvis.util.Sha512.sha512(cred);
        cred = qxelvis.util.Sha512.sha512(salt + cred);
        var rpc2 = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "ServiceSession.py", "ServiceSession");

        rpc2.addListenerOnce("completed", function(e) {
          var rep = e.getData()['result'];
          if (rep) {
            this.fireEvent('authenticationSucced');
            this.requestUserInfo();
            this.requestGroupsInfo();
          } else {
            this.fireEvent('authenticationFailed');
          }
        }, this);
        rpc2.callAsyncListeners(true, "userLogin", this.getSessionId(), lg, cred);
      }, this);
      rpc.callAsyncListeners(true, "getSalt", this.getSessionId());
    },

    /**
     * Disconnect a user.
     */
    userLogout : function() {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "ServiceSession.py", "ServiceSession");
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("connectionFailed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.requestUserInfo();
        this.requestGroupsInfo();
        this.fireEvent("userLoggedOut");
      }, this);
      rpc.callAsyncListeners(true, "userLogout", this.getSessionId());
    },

    /**
     * Get new salt value.
     */
    getSaltValue : function() {
      var rpc = new qx.io.remote.Rpc(qxelvis.session.service.Session.SERVICE_BASE_URL + "ServiceSession.py", "ServiceSession");
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("connectionFailed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        this.fireDataEvent("newSalt", e.getData()['result']);
      }, this);
      rpc.callAsyncListeners(true, "getSalt", this.getSessionId());
    },

    /**
     * Get a unique preference id for the user of that application
     */
    getPreferenceId : function() {
      var userId = "----";
      var ui = this.getUserInfo();
      if (ui != null) {
        userId = this.getUserInfo().getId();
      }
      var prefId = qxelvis.session.service.Session.APPLICATION_ID_KEY + "-" + userId;
      return qxelvis.util.Sha512.sha512(prefId);
    }
  }
});
