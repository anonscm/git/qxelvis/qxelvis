/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("qxelvis.theme.Theme",
{
  meta :
  {
    color : qxelvis.theme.Color,
    decoration : qxelvis.theme.Decoration,
    font : qxelvis.theme.Font,
    icon : qx.theme.icon.Tango,
    appearance : qxelvis.theme.Appearance
  }
});