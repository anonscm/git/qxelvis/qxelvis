/* ************************************************************************

   Copyright:

   License:

   Authors:

************************************************************************ */

qx.Theme.define("qxelvis.theme.Appearance",
{
  extend : qx.theme.modern.Appearance,

  appearances :
  {
    "grouplistitem": "atom",
    "grouplistitem/read/icon": "checkbox/icon",
    "grouplistitem/write/icon": "checkbox/icon"
  }
});
