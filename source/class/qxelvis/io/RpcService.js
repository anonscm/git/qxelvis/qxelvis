/* ************************************************************************

  qxelvis

  License:
    CeCILL: http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
    See the LICENSE file in the project's top-level directory for details.

  Copyright: 2012 INRA http://www.inra.fr

  Authors:
    * Nicolas Riault
    * Sylvain Gaillard
    * Francois Vallee

************************************************************************ */

/**
 * Generic service for all JSON-RPC access. This class manage the exchanges
 * in JSON-RPC.
 * Results of a request should be accessed by the getResult method.
 * Errors in a request should be accessed by the getError method.
 *
 */
qx.Class.define("qxelvis.io.RpcService", {
  extend : qx.core.Object,

  // --------------------------------------------------------------------------
  // [Constructor]
  // --------------------------------------------------------------------------
  /**
   *
   * @param url {String} Url of the service to call.
   * @param serviceName {String} Name of the service called.
   * @param method {String} Name of the method called.
   * @param args {Array} Arguments given to the method.
   * @param exec {Boolean} Optional argument to send the request automatically.
   */
  construct : function(url, serviceName, method, args, exec) {
    // Set default value of exec to true
    exec = typeof exec !== 'undefined' ? exec : true;

    this.base(arguments);
    this.__rpc = new qx.io.remote.Rpc(url, serviceName);
    this.__rpc.setTimeout(1000000);
    //this.__rpc.setCrossDomain(false);
    //this.__rpc.setServerData(null);
    //this.__rpc.setUseBasicHttpAuth(false);
    //this.__rpc.setUsername(null);
    //this.__rpc.setPassword(null);

    this.__serviceMethod = method;
    this.__serviceParams = args;

    if (exec == true)
      this.executeRequest();
  },

  // --------------------------------------------------------------------------
  // [Properties]
  // --------------------------------------------------------------------------
  properties : {
    /**
     * The response model is a map which contains an id, an error and a result
     * keys.
     */
    responseModel : {
      check    : "Object",
      nullable : true,
      event    : "changeResponseModel"
    }
  },


  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */
  members : {
    __rpc : null, // qx.io.remote.Rpc
    __serviceMethod : null, // String
    __serviceParams : null, // Array
    __opaqueCallReference : null,

    /**
     * Execute the request with exception catching.
     *
     */
    executeRequest : function() {
      try {
        this.sendRequest();
      }
      catch(e) {
        this.warn(e);
      }
    },

    /**
     * Main function for the RPC. Do the request call which is asynchronous.
     * In any case, the __responseModel is changed with a response variable
     * composed of {id, result, error}.
     */
    sendRequest : function() {
      /*
         Context configuration: it is necessary to make a variable
         with the current object to give the correct reference in the handler function.
      */
      var that = this;

      // Function which handles the result of the rpc.callAsync.
      var handler = function(result, error, id) {
        //that.debug(qx.lang.Json.stringify(result));
        var response;
        if (error == null) {
          response =
            {
              "id"     : id,
              "result" : result,
              "error"  : error,
              "args"  : that.getParams()
            };
        } else {
          that.showErrorDetails(error);
          response =
            {
              "id": id,
              "result" : null,
              "error" : that.handleError(error.code),
              "args"  : that.getParams()
            }
        }
        // Updating the response model
        that.setResponseModel(response);
      }; // end of handler function

      // Checking the __serviceParams attribute
      if (this.__serviceParams == null)
        var args = [ handler, this.__serviceMethod ];
      else
        var args = [ handler, this.__serviceMethod ].concat(this.__serviceParams);

      //this.debug("RpcService.handler: " + args);
      this.__opaqueCallReference = this.__rpc.callAsync.apply(this.__rpc, args);
    },

    /**
     * Main function for the RPC. Do the request call which is asynchronous.
     * In any case, the __responseModel is changed with a response variable
     * composed of {id, result, error}.
     * @deprecated {} use executeRequest instead
     * @return {Object | null} The result of the request.
     */
    executeSyncRequest : function() {
      this.warn("RPC sync request breaks apllication flow. Please don't use it!");
      try {
        if (this.__serviceParams == null) {
          var args = [ this.__serviceMethod ];
        } else {
          var args = [ this.__serviceMethod ].concat(this.__serviceParams);
        }
        return this.__rpc.callSync.apply(this.__rpc, args);
      }
      catch (e) {
        this.warn(e);
        return null;
      }
    },

    /**
     * Return a message displayed to the user.
     *
     * @param code {String} Error code
     * @return {String} The error message corresponding to the code
     */
    handleError : function (code) {
      var message;
      switch(code) {
      case "IntegrityError" :
       message = "Data already exists"; break;
      default:
        message = "Other error: " + code; break;
      }
      return message;
    },

    /**
     * Display an error message with details.
     *
     * @param details {Object} Error object
     */
    showErrorDetails : function(details) {
      this.debug("origin: " + details.origin +
                 "; code: " + details.code +
                 "; message: " + details.message);
    },

    /**
     * Reinitialize the method of the service to call.
     *
     * @param method {String} Method name.
     */
    setMethod : function(method) {
      this.__serviceMethod = method;
    },

    /**
     * Reinitialize the parameters given to the method.
     *
     * @param params {Array} Array of parameters.
     */
    setParams : function(params) {
      this.__serviceParams = params;
    },

    /**
     * Return the parameters.
     *
     * @return {Array} Array of parameters.
     */
    getParams : function() {
      return this.__serviceParams;
    },

    /**
     * Return the Qooxdoo RPC object.
     *
     * @return {qx.io.remote.Rpc} The current RPC.
     */
    getRpc : function() {
      return this.__rpc;
    },

    /**
     * Return the result attribute of the ResponseModel property.
     *
     * @return {object} The result of a request.
     */
    getResult : function () {
      if (this.getResponseModel() != null)
        return this.getResponseModel()["result"];
      else
        return null;
    },

    /**
     * Return the error attribute of the ResponseModel property.
     *
     * @return {object} The error returned by a request.
     */
    getError : function () {
      if (this.getResponseModel() != null)
        return this.getResponseModel()["error"];
      else
        return null;
    },

    /**
     * Test if the error attribute of the ResponseModel property exists.
     *
     * @return {Boolean} The state of error.
     */
    noError : function () {
      if (this.getResponseModel() != null) {
        if (this.getResponseModel()["error"] == null)
          return true;
        else
          return false;
      }
    },

    /**
     * Abort the current request
     */
     abort : function() {
       if (this.__rpc && this.__opaqueCallReference) {
         this.__rpc.abort(this.__opaqueCallReference);
       }
     }
  }
});
