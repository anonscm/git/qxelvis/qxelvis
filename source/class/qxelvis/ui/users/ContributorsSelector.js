/* ************************************************************************

  Copyright: 2015 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Pierre Brizard, bioinfo team, IRHS
    * Sandra Pelletier, bioinfo team, IRHS

************************************************************************ */

qx.Class.define("qxelvis.ui.users.ContributorsSelector",
{
  extend : qx.ui.core.Widget,

  events : {
    selectedContributors : "qx.event.type.Data"
  },

  properties : {
    model : { // TODO : necessaire ?
      init : {},
      nullable : true,
      event : "changeModel"
    },
    users : {
      init : [],
      nullable : true,
      event : "changeUsers"
    },
    contributors : {
      nullable : true,
      event : "changeContributors"
    }
  },

  construct : function() {
    this.base(arguments);
    this._setLayout(new qx.ui.layout.HBox(10));

    //GroupBox avec les contributeurs
    this.__contributorsList = new qx.ui.form.List();
    this.__contributorsList.setRequired(true);
    this.__contributorsList.setWidth(250);
    this.__contributorsList.setMaxHeight(125);
    this._add(this.__contributorsList);

    //Groupbox : "Add" and "Delete" buttons
    var addDeleteContainer = new qx.ui.container.Composite(new qx.ui.layout.VBox(10).set({alignY: "middle"}));
    var addUserButton = new qx.ui.basic.Image("qxelvis/icon/previous1.png");
    var removeUserButton = new qx.ui.basic.Image("qxelvis/icon/next1.png");
    addDeleteContainer.add(removeUserButton);
    addDeleteContainer.add(addUserButton);
    addUserButton.addListener("click", this.addUserToList, this);
    removeUserButton.addListener("click", this.removeUserFromList, this);
    this._add(addDeleteContainer);

    //GroupBox recherche d'utilisateurs
    var searchUserGroupbox = new qx.ui.container.Composite();
    searchUserGroupbox.setLayout(new qx.ui.layout.VBox(5));
    this.__searchTextField = new qx.ui.form.TextField();
    this.__searchTextField.setPlaceholder(this.tr("An user email ..."));
    this.__searchTextField.addListener("keyup", this.lookForUser, this);
    searchUserGroupbox.add(this.__searchTextField);
    this.__resultList = new qx.ui.form.List();
    searchUserGroupbox.add(this.__resultList);
    this._add(searchUserGroupbox);

    this.addListener("changeUsers", function(e) {
      this.getUsers().forEach(function(i) {
        var t = i.firstname +" "+ i.lastname;
        var dico = this.getModel();
        dico[t] = i.id;
      }, this);
    }, this);
  },

  members : {

    /**
     * La liste des contributeurs disponibles
     */
    __contributorsList : null,

    /**
     * La liste des contributeurs sélectionnés
     */
    __resultList : null,

    /**
     * Variable stockant les id des contributeurs sélectionnés
     * TODO: A SUPPRIMER
     */
    __contributorsId : null,

    /**
     * Champ text d'entrée du filtre
     */
    __searchTextField : null,

    /**
     * Filter users to add in __resultList
     */
    lookForUser : function() {
      this.__resultList.removeAll();
      var data = this.__searchTextField.getValue();
      if (data != null && data != "") {
        //console.log(this.getUsers())
        this.getUsers().forEach(function(i) {
          //console.log(i)
          if (i.firstname != null & i.lastname != null) {
            var t = i.firstname + " " + i.lastname;
            if (i.email!=null) {
              var index1 = i.email.toLowerCase().indexOf(data.toLowerCase());
            } else {var index1 = -1}
            var index2 = i.firstname.toLowerCase().indexOf(data.toLowerCase());
            var index3 = i.lastname.toLowerCase().indexOf(data.toLowerCase());
            var index4 = i.login.toLowerCase().indexOf(data.toLowerCase());
            var s = index1 + index2 + index3 + index4
            //console.log(s)
            if (s > -4) {
              var item = new qx.ui.form.ListItem(t);
              this.__resultList.add(item);
              item.addListener("dblclick", this.addUserToList, this);
            }
          }
        }, this);
      } else { this.__resultList.removeAll(); }
    },

    /**
    * Add a user in the contributors list
    */
    addUserToList : function() {
      var user = this.__resultList.getSelection()[0].getLabel()
      var addUser = true;
      if (this.__contributorsList.getChildren().length > 0) {
        for (var j = 0; j <  this.__contributorsList.getChildren().length; j++) {
          if (this.__contributorsList.getChildren()[j].getLabel() == user) {addUser = false;}
        }
        if (addUser) {
          var contributor = new qx.ui.form.ListItem(user);
          contributor.addListener("dblclick", this.removeUserFromList, this);
          this.__contributorsList.add(contributor);
        }
      } else {
        var contributor = new qx.ui.form.ListItem(user);
        contributor.addListener("dblclick", this.removeUserFromList, this);
        this.__contributorsList.add(contributor);
      }
      return this._getContributors();
    },

    /**
    * Delete a user from the contributors list
    */
    removeUserFromList : function() {
      var user = this.__contributorsList.getSelection()[0].getLabel();
      var j = 0
      this.__contributorsList.getChildren().forEach(function(i) {
        if (i.getLabel() == user) {
          this.__contributorsList.remove(i);
        } else {j++;}
      }, this);
      this._getContributors()
    },

    _getContributors : function() {
      this.__contributorsId = [];
      this.__contributorsList.getChildren().forEach(function(i) {
        this.__contributorsId.push(this.getModel()[i.getLabel()]);
      }, this);
      this.setContributors(this.__contributorsId);
    }
  } // end members
});
