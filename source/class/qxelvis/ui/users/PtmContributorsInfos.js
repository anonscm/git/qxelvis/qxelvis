/* ************************************************************************

  Copyright: 2015 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Sandra Pelletier, bioinfo team, IRHS, 2017-09-20

************************************************************************ */

qx.Class.define("qxelvis.ui.users.PtmContributorsInfos",
{
  extend : qx.ui.core.Widget,
  construct : function(data, projet, pointerLeft, pointerTop) {
    this.base(arguments);
    /*
    // data
    {
      "name_type": "Responsable",
      "firstname": "Aur\u00e9lie",
      "lastname": "Leli\u00e8vre",
      "email": "",
      "phone": null,
      "id_type": 1,
      "active": false,
      "login": "aurelie",
      "id": 1005
    }
    */
    // TODO : Rattachement Equipe
    
    //console.log(data.getFirstname());
    var win = new qx.ui.window.Window(data.getFirstname() + " " + data.getLastname());
    var layout = new qx.ui.layout.Grid(10, 4);
    layout.setColumnWidth(2, 200);
    layout.setRowHeight(1, 20);
    layout.setColumnAlign(0, "right", "top");
    win.setLayout(layout);
    
    win.add(new qx.ui.basic.Label("Contributor :"), {row: 0, column: 0});
    win.add(new qx.ui.basic.Label("Email :"), {row: 2, column: 0});
    win.add(new qx.ui.basic.Label("Phone :"), {row: 3, column: 0});
    win.add(new qx.ui.basic.Label("Infos ELVIS :"), {row: 4, column: 0});
    win.add(new qx.ui.basic.Label("login :"), {row: 4, column: 1});
    win.add(new qx.ui.basic.Label("id :"), {row: 5, column: 1});
    win.add(new qx.ui.basic.Label("active :"), {row: 6, column: 1});
    
    var userValue = data.getFirstname() + " " + data.getLastname();
    userValue = (userValue != null) ? userValue : "<i style='color: grey;'>no name</i>";
    var user = new qx.ui.basic.Label(userValue);
    win.add(user, {row: 0, column: 1, colSpan: 2});
    
    var typeValue = data.getName_type();
    typeValue = (typeValue != null) ? typeValue : "<i style='color: grey;'>no task</i>";
    typeValue = typeValue + " du projet " + projet;
    var type = new qx.ui.basic.Label(typeValue);
    win.add(type, {row: 1, column: 1, colSpan: 2});
    
    var emailValue = data.getEmail();
    emailValue = (emailValue == null||emailValue == "") ? 
      "<i style='color: grey;'>no email</i>" : emailValue;
    var email = new qx.ui.basic.Label();
    email.setRich(true);
    email.setValue(emailValue);
    win.add(email, {row: 2, column: 1, colSpan: 2});
    
    var phoneValue = data.getPhone();
    phoneValue = (phoneValue != null) ? phoneValue : "<i style='color: grey;'>no phone</i>";
    var phone = new qx.ui.basic.Label();
    phone.setRich(true);
    phone.setValue(phoneValue);
    win.add(phone, {row: 3, column: 1, colSpan: 2});
    
    var loginValue = data.getLogin();
    loginValue = (loginValue != null) ? loginValue : "<i style='color: grey;'>no login</i>";
    var login = new qx.ui.basic.Label(loginValue);
    win.add(login, {row: 4, column: 2});
    
    var idValue = data.getId();
    idValue = (idValue != null) ? idValue : "<i style='color: grey;'>no id</i>";
    var id = new qx.ui.basic.Label(idValue.toString());
    win.add(id, {row: 5, column: 2});
    
    var activeValue = data.getActive();
    if (activeValue) { activeValue = "<i style='color: green;'>" + activeValue + "</i>"; }
    else { activeValue = "<i style='color: red;'>" + activeValue + "</i>"; }
    var active = new qx.ui.basic.Label();
    active.setRich(true);
    active.setValue(activeValue);
    win.add(active, {row: 6, column: 2});
    
    win.setWidth(300);
    //win.setHeight(200);
    win.setShowMinimize(false);
    win.moveTo(pointerLeft, pointerTop);
    win.open();
  },
  
  members : {
  }// members
});

