/* ************************************************************************

  Copyright: 2015 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Sandra Pelletier, bioinfo team, IRHS

************************************************************************ */

qx.Class.define("qxelvis.ui.users.UserSelector",
{
  extend : qx.ui.core.Widget,
  implement : [qx.ui.form.IModel],
  include : [qx.ui.form.MModelProperty],

  events : {
    selectedActiveUser : "qx.event.type.Data"
  },

  properties : {
    activeUser : {
      nullable : true,
      event : "changeActiveUser"
    },
    manager : {
      nullable : true,
      event : "changeManager"
    },
    id : {
      nullable : true,
      event : "changeId"
    }
  },

  construct : function() {
    this.base(arguments);
    this._setLayout(new qx.ui.layout.HBox(10));
    
    var userChooser = new qxelvis.ui.AutoCompleteBox();
    userChooser.setMinWidth(250);
    userChooser.setPlaceholder(this.tr("Select an agent"));
    this._add(userChooser);
    
    this.addListener("changeActiveUser", function(e) {
      var rawResult = e.getData();
      this.__item = {};
      this.__userId = {};
      if (rawResult) {
        for (var i = 0; i < rawResult.length; i++) {
          var t = rawResult[i].firstname + " " + rawResult[i].lastname
          this.__item[t] = t + rawResult[i].email + rawResult[i].login
          this.__userId[t] = rawResult[i].id
        }
      }
      userChooser.setListModel(this.__item);
    }, this);
    
    userChooser.bind("value", this, "manager");
    this.addListener("changeManager", function(e) {
      if (Object.keys(this.__item).indexOf(this.getManager()) != -1 & this.getManager() != null & this.getManager() != "") {
        this.setId(this.__userId[this.getManager()]);
      } else {
        this.setId(null);
      }
    }, this);
  }, 
  
  members : {
    __userId : null,
    __item : {}
  }
});
