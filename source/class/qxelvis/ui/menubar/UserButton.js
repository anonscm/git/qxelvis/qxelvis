/**
 * A user menu button
 *
 * This User menu button is linked to a qxelvis session.
 * It allows user to login/logout and access its account data.
 */
qx.Class.define("qxelvis.ui.menubar.UserButton", {
  extend : qx.ui.core.Widget,

  events : {
    /**
     * Fired when the UserButton is clicked
     */
    execute : "qx.event.type.Event",

    /**
     * Fired when the 'Account' entry is clicked
     */
    wantUserAccount : "qx.event.type.Event",

    /**
     * Fired when the UserButton is clicked and no user is logged into the session
     */
    wantLogin : "qx.event.type.Event",

    /**
     * Fired when the 'Logout' entry is clicked
     */
    wantLogout : "qx.event.type.Event"
  },

  construct : function(icon) {
    this.base(arguments);
    this._setLayout(new qx.ui.layout.Grow());

    var session = qxelvis.session.service.Session.getInstance();

    var loginBtn = new qx.ui.menubar.Button(this.tr("Loading..."), icon);

    var usrMenu = new qx.ui.menu.Menu();

    var accountBtn = new qx.ui.menu.Button(this.tr("Account"));
    accountBtn.addListener("execute", function() {
      this.fireEvent("wantUserAccount");
    }, this);

    var logoutBtn = new qx.ui.menu.Button(this.tr("Logout"));
    logoutBtn.addListener("execute", function() {
      if (session.userLoggedIn()) {
        this.fireEvent("wantLogout");
        session.userLogout();
      }
    }, this);

    usrMenu.add(accountBtn);
    usrMenu.add(logoutBtn);
    loginBtn.setMenu(usrMenu);

    session.addListener("gotUserInfo", function(e) {
      var user = session.getUserInfo();
      var loginValue = this.tr("Login");
      if (user != null) {
        loginValue = user.getDisplayName();
        loginBtn.setMenu(usrMenu);
      } else {
        loginBtn.resetMenu();
      }
      loginBtn.setLabel(loginValue);
    }, this);

    this._add(loginBtn);
    loginBtn.addListener("execute", function() {
      var user = session.getUserInfo();
      if (user == null) {
        this.fireEvent("wantLogin");
      }
      this.fireEvent("execute");
    }, this);
  }
});
