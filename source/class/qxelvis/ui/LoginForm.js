qx.Class.define("qxelvis.ui.LoginForm", {
  extend : qx.ui.core.Widget,

  properties : {
    validLogin : {
      event : "changeValidLogin",
      nullable: false,
      init : true,
      apply : "__applyValidLogin"
    },
    validPassword : {
      event : "changeValidPassword",
      nullable: false,
      init : true,
      apply : "__applyValidPassword"
    }
  },

  events : {
    /**
     * Fired when widget is validated
     */
    "validate" : "qx.event.type.Data",

    /**
     * Fired when widget is reseted
     */
    "reset" : "qx.event.type.Event"
  },

  construct : function() {
    this.base(arguments);
    this._setLayout(new qx.ui.layout.VBox(10));
    this.__usernameFld = new qx.ui.form.TextField();
    this.__passwordFld = new qx.ui.form.PasswordField();
    var username = this.__usernameFld;
    var password = this.__passwordFld;
    username.setPlaceholder(this.tr("Identifiant"));
    password.setPlaceholder(this.tr("Mot de passe"));
    username.setLiveUpdate(true);
    password.setLiveUpdate(true);

    this.bind("validLogin", username, "valid");
    this.bind("validPassword", password, "valid");

    this.addListener("focus", function() {
      username.focus();
    }, this);

    username.addListener("focusout", function() {
      //this.debug("username loose capture");
      password.focus();
    }, this);
    password.addListener("focusout", function() {
      //this.debug("password loose capture");
      username.focus();
    }, this);

    this.addListener("appear", function() {
      username.focus();
    }, this);

    this.addListener("keypress", function(e) {
      if (e.getKeyIdentifier() == "Escape") {
        this.reset();
      }
    }, this);

    username.addListener("keypress", function(e) {
      if (e.getKeyIdentifier() == "Enter") {
        if (this.__password) {
          this.validate();
        } else {
          password.focus();
        }
      }
    }, this);
    password.addListener("keypress", function(e) {
      if (e.getKeyIdentifier() == "Enter") {
        this.validate();
      }
    }, this);
    username.addListener("changeValue", function(e) {
      this.setValidLogin(true);
      this.__username = e.getData();
    }, this);
    password.addListener("changeValue", function(e) {
      this.setValidPassword(true);
      this.__password = e.getData();
    }, this);

    this._add(username);
    this._add(password);
  },

  members : {
    __password : null,
    __username : null,
    __passwordFld : null,
    __usernameFld : null,
    __valid : null,

    validate : function() {
      this.setValidLogin(Boolean(this.__username));
      var val = this.__username && this.__password;
      if (val) {
        this.fireDataEvent("validate", {"username" : this.__username, "password" : this.__password});
      }
    },

    setValid : function(value) {
      this.setValidLogin(value);
      this.setValidPassword(value);
      this.__valid = value;
    },

    reset : function() {
      this.__usernameFld.setValue(null);
      this.__passwordFld.setValue(null);
      this.setValid(true);
      this.fireEvent("reset");
    },

    __applyValidLogin : function(value, old) {
      this.__valid = value;
    },

    __applyValidPassword : function(value, old) {
      this.__valid = value;
    }
  }
});
