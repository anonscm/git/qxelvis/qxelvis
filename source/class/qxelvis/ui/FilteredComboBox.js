/* ************************************************************************

  Copyright: 2018 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/LicenceCeCILLV2-en.html
    See the LICENCE file in the project's top-level directory for details.

  Authors:
    * Fabrice Dupuis, Bioinfo team, IRHS
    * Julie Bourbeillon, Bioinfo team, IRHS
    * Pierre Brizard, Bioinfo team, IRHS
    * Sylvain Gaillard, Bioinfo team, IRHS

*************************************************************************/

/**
 * A VirtualComboBox with filter on the dropdown list.
 *
 * The value entered in the textfield is used has a filter applied to the
 * dropdown list of options of the combobox.
 */

qx.Class.define("qxelvis.ui.FilteredComboBox",
{
  extend : qx.ui.form.VirtualComboBox,

  /**
   * Events definitions.
   */
  events : {
    /**
     * Fired when the selection in the combobox changes
     *
     * This event contains the selected item in the combobox
     */
    "changeSelection" : "qx.event.type.Data",

    "changeSelectedString": "qx.event.type.Data",
    "changeAllValues": "qx.event.type.Data"
  },

  /**
   * Constructor
   */

  /**
   * @param array {Array} The array containing the data on wich the combobox is based
   * in the form [{"label" : "Apple", "id" : 1}, {"label" : "Orange", "id" : 2}].
   */
  construct : function(array) {
    this.base(arguments);

    // Set "label" in the data model as the default value to display in the dropdown
    // TODO: must be used as key for filtering and event firing test.
    this.setLabelPath("label");
    this.setSelectable(true);

    if (array) {
      this.setListModel(array);
    }

    //Listener to monitor changes in the textfield
    this.getChildControl("textfield").addListener("input", function(e) {
      var text = e.getData();
      if (text.length >= 1) {
        this.fireDataEvent("changeSelectedString", text);

        // filtre les items
        var delegate = {
          filter: function(item) {
            var index = item.getLabel().toLowerCase().indexOf(String(text).toLowerCase());
            return index >= 0;
          }
        };
        this.setDelegate(delegate);
      } else {
        var delegate = {
          filter: function(item) {
            return true;
          }
        };
        this.setDelegate(delegate);
        this.updateAppearance();
      }
      this.open();
    }, this);

    this.addListener("changeValue", function(e) {
      if (this.__lastSelection && e.getData() == this.__lastSelection.getLabel()) {
        this.fireDataEvent("changeSelection", this.__lastSelection);
      }
    }, this);

    // Listener to monitor selection change in the dropdown
    this.getChildControl("dropdown").getChildControl("list").getSelection().addListener("change", function(e) {
      //this.debug("Selection changed");
      var sel = e.getData();
      if (sel["added"] && sel["added"].length > 0) {
        this.__lastSelection = e.getData()["added"][0];
      }
      //this.fireDataEvent("changeSelection", e.getData());
    }, this);
  },

  /**
   * Method definitions.
   */
  members : {

    __lastSelection : null,

    /**
     * Set list model
     *
     * Use the Array provided as argument to define the data model of the list
     * @param array {Array} The array containing the data on wich the filteredi
     * combobox is based
     */
    setListModel : function(array) {
      var model = qx.data.marshal.Json.createModel(array);
      this.setModel(model);
    }
  }
});
