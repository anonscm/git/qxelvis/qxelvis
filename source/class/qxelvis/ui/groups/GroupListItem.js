/**
 * GroupListItem class
 *
 * This class is used to display group rights in a GroupList
 */
qx.Class.define("qxelvis.ui.groups.GroupListItem", {
  extend : qx.ui.core.Widget,

  properties : {
    appearance : {
      refine : true,
      init : "grouplistitem"
    },

    /**
     * Store the group id
     */
    id : {
      nullable : true,
      apply : "_applyId"
    },

    /**
     * Store the group name
     */
    name : {
      nullable : true,
      apply : "_applyName",
      event : "changeName"
    },

    /**
     * Store the group description
     */
    description : {
      nullable : true,
      apply : "_applyDescription"
    },

    /**
     * Store the group rights as a binary flag (1 = read, 2 = write)
     */
    mask : {
      nullable : true,
      apply : "_applyMask",
      event : "changeMask"
    }
  },

  construct : function() {
    this.base(arguments);
    var layout = new qx.ui.layout.Grid(1,3);
    layout.setColumnFlex(0, 1);
    this._setLayout(layout);

    this._createChildControl("name");
    this._createChildControl("read");
    this._createChildControl("write");
  },

  members : {
    _createChildControlImpl : function(id, hash) {
      var control;

      switch (id) {
        case "name":
          control = new qx.ui.basic.Label(this.getName() + ' (' + this.getId() + ')');
          control.setAnonymous(true);
          this._add(control, {row: 0, column: 0});
          break;

        case "read":
          control = new qx.ui.form.CheckBox();
          control.setValue(Boolean(this.getMask() & 1));
          control.addListener("changeValue", function(e) {
            if (this.getId()) {
              this.__setMask();
            }
          }, this);
          this._add(control, {row: 0, column: 1});
          break;

        case "write":
          control = new qx.ui.form.CheckBox();
          control.setValue(Boolean(this.getMask() & 2));
          control.addListener("changeValue", function(e) {
            if (this.getId()) {
              this.__setMask();
            }
          }, this);
          this._add(control, {row: 0, column: 2});
          break;
      }

      return control || this.base(arguments, id);
    },

    _applyId : function(value, old) {
      this._applyName(this.getName());
    },

    _applyName : function(value, old) {
      var name = this.getChildControl("name");
      //name.setValue(value + ' (' + this.getId() + ')');
      name.setValue(value);
    },

    _applyDescription : function(value, old) {
    },

    _applyMask : function(value, old) {
      var write = this.getChildControl("write");
      var read = this.getChildControl("read");
      read.setValue(Boolean(value & 1));
      write.setValue(Boolean(value & 2));
    },

    /**
     * Convert the checkboxes value to the correspondig right flag
     */
    __setMask : function() {
      var mask = 0;
      var write = this.getChildControl("write");
      var read = this.getChildControl("read");
      if (read.getValue()) {
        mask |= 1;
      }
      if (write.getValue()) {
        mask |= 2;
      }
      this.setMask(mask);
    }
  }
});
