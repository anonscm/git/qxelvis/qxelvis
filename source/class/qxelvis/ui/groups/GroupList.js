qx.Class.define("qxelvis.ui.groups.GroupList", {
  extend : qx.ui.core.Widget,

  properties : {
    groups : {
      nullable : true,
      event : "changeGroups"
    }
  },

  construct : function() {
    this.base(arguments);
    var list = new qx.ui.form.List();
    //list.setAnonymous(true);

    this._setLayout(new qx.ui.layout.Grow());
    this._add(list);

    var session = qxelvis.session.service.Session.getInstance();
    list.addListener("focusout", function() {
      var gis = session.getUserGroupsInfo();
      gis.saveUserPreferences();
    }, this);
    session.addListener("gotGroupsInfo", function(e) {
      list.removeAll();
      list.add(this.__createHeader());
      var gis = session.getUserGroupsInfo();
      if (gis != null) {
        var allGrps = gis.getAllGroupIds();
        var that = this;
        allGrps.forEach(function(item) {
          var g = gis.getGroup(item);
          if (g) {
            var it = new qxelvis.ui.groups.GroupListItem();
            it.setName(g['name']);
            it.setId(g['id']);
            it.setMask(g['mask']);
            it.setDescription(g['description']);
            list.add(it);
            it.addListener("changeMask", function(e) {
              var gi = e.getTarget();
              var masks = {};
              masks[gi.getId()] = gi.getMask();
              gis.setGroupsRights(masks);
            }, that);
          }
        });
      }
    }, this);
  },

  members : {
    __createHeader : function() {
      var h = new qx.ui.container.Composite();
      h.setLayout(new qx.ui.layout.Grid(1, 3));
      h.getLayout().setColumnFlex(0, 1);
      h.add(new qx.ui.basic.Label('r').set({width : 14, textAlign : "center"}), {row : 0, column : 1});
      h.add(new qx.ui.basic.Label('w').set({width : 14, textAlign : "center"}), {row : 0, column : 2});
      return h;
    }
  }
});
