/**
    Autocomplete Box

    DEPRECATED CLASS
    
    Authors: Pierre Brizard
*/
qx.Class.define("qxelvis.ui.AutoCompleteBox",
{
  extend : qx.ui.form.VirtualComboBox,

  events : {
    "changeSelection" : "qx.event.type.Event"
  },

  /**
  * Autocomplete Box
  */
  construct : function(array) {
    this.base(arguments);
    this.warn("This class is deprecated, use qxelvis.ui.FilteredComboBox instead");
    if (array) { this.setListModel(array); }

    //Recherche listener
    this.getChildControl("textfield").addListener("input", function(e) {
      var filteredData = new qx.data.Array();

      //Recherche  et filtre les éléménts contenant le texte entré
      for (var i in this.__dico) {
        if (i != null) {
          var index = this.__dico[i].toLowerCase().indexOf(e.getData().toLowerCase());
          if (index > -1) {
            filteredData.push(i);
          }
        }
      }

      //Met à jour le nouveau modèle
      var modelNew = qx.data.marshal.Json.createModel(filteredData);
      this.setModel(modelNew);

      //Règle la taille de la liste
      var dropdown = this.getChildControl("dropdown");
      var list = dropdown.getChildControl("list");
      list.setHeight(null);
      list.setMaxHeight(100);
      list.setMinHeight(0);
      if (filteredData.length > 0) {
        this.open();
      }
    }, this)
  },
  members : {

    setListModel : function(array) {
      var test = array;
      this.__dico = array;
      var listUser = [];
      for (var i in this.__dico) {
        listUser.push(i);
      }
      this.__data = listUser;
      var model = qx.data.marshal.Json.createModel(listUser);
      this.setModel(model);
    },

    validate : function() {
      if (this.__data.indexOf(this.getValue()) > -1 || this.getValue() == null || this.getValue() == "") {
        this.setValid(true);
        return true;
      }
      this.setValid(false);
      return false;
    }
  }
});
