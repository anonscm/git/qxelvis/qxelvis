/* ************************************************************************

  qooxdoo - the new era of web development
  http://qooxdoo.org

  Copyright: 2017 INRA http://www.inra.fr

  License:
    CeCILL: http://www.cecill.info/licences/Licence_CeCILL_V2-en.html
    See the LICENSE file in the project's top-level directory for details.

  Authors:
    * Sylvain Gaillard
   
************************************************************************ */

qx.Class.define("qxelvis.util.UUID",
{
  statics :
  {
    /**
     * Generate a new UUID, RFC4122 version 4 compliant.
     */
    generate : function () {
      if (this.__hex.length == 0) {
        this.__hexInitiate();
      }
      var d3 = Date.now();
      if (window.performance && typeof window.performance.now === "function") {
        d3 += window.performance.now(); //use high-precision timer if available
      }

      var d0 = Math.random()*0xffffffff|0;
      var d1 = Math.random()*0xffffffff|0;
      var d2 = Math.random()*0xffffffff|0;

      return this.__hex[d0&0xff]+this.__hex[d0>>8&0xff]+this.__hex[d0>>16&0xff]+this.__hex[d0>>24&0xff]+'-'+
        this.__hex[d1&0xff]+this.__hex[d1>>8&0xff]+'-'+this.__hex[d1>>16&0x0f|0x40]+this.__hex[d1>>24&0xff]+'-'+
        this.__hex[d2&0x3f|0x80]+this.__hex[d2>>8&0xff]+'-'+this.__hex[d2>>16&0xff]+this.__hex[d2>>24&0xff]+
        this.__hex[d3&0xff]+this.__hex[d3>>8&0xff]+this.__hex[d3>>16&0xff]+this.__hex[d3>>24&0xff];
    },

    /**
     * Hex lookup table.
     */
    __hex : [],

    /**
     * Initiate the hex lookup
     */
    __hexInitiate : function() {
      for (var i = 0 ; i < 256 ; i++) {
        this.__hex[i] = (i < 16 ? '0' : '') + (i).toString(16);
      }
    }

  }
});
