/**
 * Service
 * En cours de développement !!!
 */
qx.Class.define("qxelvis.service.plant.ServiceLot",
{
  type : "singleton",
  extend : qx.core.Object,

  events : {
    "failed" : "qx.event.type.Data",
    "completed" : "qx.event.type.Data"
  },

  /**
  * Constructor
  */
  construct : function() {
    this.base(arguments);
    qxelvis.service.plant.ServiceLot.SERVICE_BASE_URL = qxelvis.session.service.Session.SERVICE_BASE_URL;
    this._serviceUrl = qxelvis.session.service.Session.SERVICE_BASE_URL + "/ServicePlant.py";
    this._serviceName = "ServicePlant";
  },

  statics : {
    SERVICE_BASE_URL : "SERVICE_BASE_URL_not_defined"
  },

  /**
  * Method definitions.
  */
  members :
  {
    _serviceUrl : "URL_NOT_SET",
    _serviceName : "SERVICE_NOT_SET",

    /**
    * asynchronous query
    *@param serviceMethod {String} method to call
    *@param args {Array} arguments
    *@return {Array} result
    */
    query : function(serviceMethod, args) {
      return new qxelvis.io.RpcService(this._serviceUrl, this._serviceName, serviceMethod, args);
    },
    getConformityLotList : function(lotList) {
      var outputData = [];
      for (var k = 0 ; k < lotList.length ; k++) {
       outputData[k] = qx.util.Serializer.toNativeObject(lotList[k]);
      }
      var session = qxelvis.session.service.Session.getInstance();
      var rpc = new qx.io.remote.Rpc(this._serviceUrl, this._serviceName);
      rpc.addListener("failed", function(e) {
        this.fireDataEvent("failed", e.getData());
      }, this);
      rpc.addListener("completed", function(e) {
        var resp = e.getData();
        this.fireDataEvent("completed", e.getData());
      }, this);
      rpc.callAsyncListeners(
        true,
        "getConformityLotList",
        // [
        session.getSessionId(),
        session.getUserGroupsInfo().getWriteGroupIds(),
        outputData
        // ]
      );
      return true;
    }
  }
});
